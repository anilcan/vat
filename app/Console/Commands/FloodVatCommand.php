<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use SoapClient;

class FloodVatCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'flood:vat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start = Carbon::now();
        $say = 1;

        while (true) {
            try {
                $client = new SoapClient('http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl', [
                    'trace' => 1
                ]);

                $response = ($client->checkVatApprox(array(
                    'countryCode' => 'FR',
                    'vatNumber' => '86443790480',
                    'requesterCountryCode' => 'FR',
                    'requesterVatNumber' => '95481749687'
                )));

                if ($response AND $response->valid) {
                    if ($say % 10 == 0) {
                        $diff = Carbon::now()->diff($start)->format('%h:%i:%s:%f');
                        $this->info("Validated {$say} times. ({$diff})");
                    }
                } else {
                    throw new \Exception($response);
                }
            } catch (\Exception $exception) {
                $diff = Carbon::now()->diff($start);
                dd([$diff, $exception, isset($response) ? $response : null]);
            }

            $say++;
        }
    }
}
