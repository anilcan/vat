<?php


namespace App\Validators;


use App\Contracts\ValidatorContract;
use App\Vat\Support\VatHelper;
use Illuminate\Validation\Validator;

class VatValidator implements ValidatorContract
{
    /**
     * @param string $attribute
     * @param string $value
     * @param array $parameters
     * @param Validator $validator
     * @return bool
     */
    public function validate($attribute, $value, $parameters, $validator): bool
    {
        $formatted = VatHelper::formatVatNumber($value);
        return preg_match(VatHelper::PATTERN, $formatted);
    }
}
