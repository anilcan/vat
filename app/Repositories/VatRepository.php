<?php


namespace App\Repositories;

use App\Models\Request;
use App\Models\Vat;
use App\Vat\EuropaClient;
use App\Vat\Support\VatHelper;
use Cache;
use Carbon\Carbon;

class VatRepository
{
    /**
     * @var EuropaClient
     */
    private $client;

    /**
     * VatRepository constructor.
     * @param EuropaClient $client
     */
    public function __construct(EuropaClient $client)
    {
        $this->client = $client;
    }

    /**
     * Find or fetch the vat number.
     *
     * @param $vatNumber
     * @param $requesterVatNumber
     * @param $userId
     *
     * @return Vat|bool
     */
    public function findOrFetch($vatNumber, $requesterVatNumber, $userId)
    {
        $vatNumber = VatHelper::formatVatNumber($vatNumber);
        $requesterVatNumber = VatHelper::formatVatNumber($requesterVatNumber);

        $attributes = [
            'vat_number' => $vatNumber,
            'requester_vat_number' => $requesterVatNumber,
            'user_id' => $userId,
            'request_file' => RequestRepository::createRequestFile()
        ];

        $request = Request::create($attributes);

        if (!$requesterVatNumber AND Cache::has($this->getCacheKey($vatNumber))) {
            /** @var Vat $vat */
            $vat = Cache::get($this->getCacheKey($vatNumber));
            $request->update(['vat_id' => $vat->id]);
            return $vat;
        }

        $response = $this->client->checkVatApproxByVatNumber($vatNumber, $requesterVatNumber);
        if ($response->valid) {
            $vat = Vat::where('vat_number', $response->getFullVatNumber())->first();
            if ($vat) {
                $vat->update([
                    'trader_name' => $response->traderName,
                    'trader_address' => $response->traderAddress,
                    'trader_company_type' => $response->traderCompanyType,
                    'last_request_at' => Carbon::now()
                ]);
            } else {
                $vat = Vat::create([
                    'vat_number' => $response->getFullVatNumber(),
                    'trader_name' => $response->traderName,
                    'trader_address' => $response->traderAddress,
                    'trader_company_type' => $response->traderCompanyType,
                    'last_request_at' => Carbon::now()
                ]);
            }

            $request->vat_id = $vat->id;
            $request->response_file = RequestRepository::createResponseFile($response);
            $request->identifier = $response->requestIdentifier;
            $request->save();
            $this->cache($vat);
            return $vat;
        }

        return false;
    }

    /**
     * Get cache key for vat number.
     *
     * @param $vatNumber
     * @return string
     */
    private function getCacheKey($vatNumber)
    {
        return "vat.{$vatNumber}";
    }

    /**
     * Cache the valid response.
     *
     * @param Vat $vat
     */
    private function cache(Vat $vat)
    {
        $key = $this->getCacheKey($vat->vat_number);
        Cache::put($key, $vat, config('vat.cache_ttl'));
    }
}
