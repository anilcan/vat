<?php


namespace App\Repositories;


use App\Vat\Models\CheckVatApproxModel;
use Illuminate\Support\Str;
use Storage;

class RequestRepository
{
    /**
     * Create a request file.
     *
     * @return string
     */
    public static function createRequestFile()
    {
        $data = [
            'headers' => request()->header(),
            'fields' => request()->all(),
            'ip' => request()->getClientIps()
        ];

        $path = 'requests/' . Str::random(63) . '.json';
        Storage::put($path, json_encode($data));

        return $path;
    }

    /**
     * Create a response file.
     *
     * @param CheckVatApproxModel $model
     * @return string
     */
    public static function createResponseFile(CheckVatApproxModel $model)
    {
        $path = 'responses/' . Str::random(63) . '.json';
        Storage::put($path, json_encode($model));

        return $path;
    }
}
