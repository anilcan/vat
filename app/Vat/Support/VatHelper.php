<?php


namespace App\Vat\Support;

class VatHelper
{
    const PATTERN = '/([A-Z]{2})([0-9|A-Z]{8,12})/';

    /**
     * Format the given vat number.
     *
     * @param $vat
     * @return string|string[]|null
     */
    public static function formatVatNumber($vat)
    {
        if (!$vat) {
            return null;
        }

        $vat = preg_replace('/\s/', '', $vat);
        $vat = preg_replace('/\W/', '', $vat);
        $vat = strtoupper($vat);

        return $vat;
    }

    /**
     * Parse a vat number.
     *
     * @param $vat
     * @return array|null
     */
    public static function parse($vat)
    {
        if (preg_match(self::PATTERN, $vat, $matches)) {
            return [
                'countryCode' => $matches[1],
                'vatNumber' => $matches[2]
            ];
        }

        return null;
    }
}
