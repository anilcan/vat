<?php

namespace App\Vat\Models;

use Carbon\Carbon;

class CheckVatApproxModel
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    public $countryCode;

    /**
     * @var string
     */
    public $vatNumber;

    /**
     * @var Carbon
     */
    public $requestDate;
    /**
     * @var bool
     */
    public $valid;

    /**
     * @var string
     */
    public $traderName;

    /**
     * @var string
     */
    public $traderCompanyType;

    /**
     * @var string
     */
    public $traderAddress;

    /**
     * @var string
     */
    public $requestIdentifier;

    /**
     * @return string
     */
    public function getFullVatNumber(): string
    {
        return $this->countryCode . $this->vatNumber;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
