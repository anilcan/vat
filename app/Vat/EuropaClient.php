<?php

namespace App\Vat;

use App\Vat\Models\CheckVatApproxModel;
use App\Vat\Support\VatHelper;
use Carbon\Carbon;
use Illuminate\Contracts\Config\Repository;
use SoapClient;
use SoapFault;

class EuropaClient
{
    const DATE_FORMAT = 'Y-m-dP';

    /**
     * The WSDL of soap.
     *
     * @var string
     */
    private $wsdl;

    /**
     * Soap client.
     *
     * @var SoapClient
     */
    private $client;

    /**
     * Config repository.
     *
     * @var Repository
     */
    private $config;

    /**
     * EuropaSoapClient constructor.
     *
     * @param Repository $config
     * @throws SoapFault
     */
    public function __construct(Repository $config)
    {
        $this->config = $config;
        $this->wsdl = $config->get('vat.wsdl');
        $this->client = new SoapClient($this->wsdl, [
            'trace' => 1
        ]);
    }

    /**
     * Check vat approx by using full vat number.
     *
     * @param $vatNumber
     * @param $requesterVatNumber
     * @return CheckVatApproxModel
     */
    public function checkVatApproxByVatNumber($vatNumber, $requesterVatNumber)
    {
        $parseVatNumber = VatHelper::parse($vatNumber);
        if ($requesterVatNumber AND $parseRequesterVatNumber = VatHelper::parse($requesterVatNumber)) {
            return $this->checkVatApprox(
                $parseVatNumber['countryCode'],
                $parseVatNumber['vatNumber'],
                $parseRequesterVatNumber['countryCode'],
                $parseRequesterVatNumber['vatNumber']
            );
        }

        return $this->checkVatApprox($parseVatNumber['countryCode'], $parseVatNumber['vatNumber']);
    }

    /**
     * Check vat approx.
     *
     * @param string $countryCode
     * @param string $vatNumber
     * @param string|null $requesterCountryCode
     * @param string|null $requesterVatNumber
     * @return CheckVatApproxModel
     */
    public function checkVatApprox(
        string $countryCode,
        string $vatNumber,
        string $requesterCountryCode = null,
        string $requesterVatNumber = null
    )
    {
        if (!$requesterCountryCode || !$requesterVatNumber) {
            $requesterCountryCode = $this->config->get('vat.default_country_code');
            $requesterVatNumber = $this->config->get('vat.default_vat_number');
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $response = $this->client->checkVatApprox([
            'countryCode' => $countryCode,
            'vatNumber' => $vatNumber,
            'requesterCountryCode' => $requesterCountryCode,
            'requesterVatNumber' => $requesterVatNumber
        ]);

        $model = $this->toCheckVatApproxModel($response);
        return $model;
    }

    /**
     * @return string
     */
    public function getWsdl(): string
    {
        return $this->wsdl;
    }

    /**
     * @return SoapClient
     */
    public function getClient(): SoapClient
    {
        return $this->client;
    }

    /**
     * Response to CheckVatApproxModel.
     *
     * @param mixed|object $response
     * @return CheckVatApproxModel
     */
    public function toCheckVatApproxModel($response)
    {
        $requestDate = Carbon::createFromFormat(self::DATE_FORMAT, $response->requestDate);

        $model = new CheckVatApproxModel();
        $model->countryCode = $response->countryCode;
        $model->vatNumber = $response->vatNumber;
        $model->requestDate = $requestDate;

        $model->valid = $response->valid;
        if ($model->valid) {
            $model->traderName = trim($response->traderName);
            $model->traderCompanyType = $response->traderCompanyType == '---' ? null : trim($response->traderCompanyType);
            $model->traderAddress = trim($response->traderAddress);
            $model->requestIdentifier = $response->requestIdentifier;
        }

        return $model;
    }
}
