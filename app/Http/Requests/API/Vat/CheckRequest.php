<?php

namespace App\Http\Requests\API\Vat;

use Illuminate\Foundation\Http\FormRequest;

class CheckRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vat_number' => 'required|vat',
            'requester_vat_number' => 'nullable|vat',
            'user_id' => 'nullable|string|max:255'
        ];
    }
}
