<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Response success for api.
     *
     * @param $data
     * @return JsonResponse
     */
    protected function apiSuccess($data)
    {
        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * Response error for api.
     *
     * @param string $message
     * @return JsonResponse
     */
    protected function apiError($message = 'Server error')
    {
        return response()->json([
            'message' => $message
        ], 500);
    }
}
