<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Vat\CheckRequest;
use App\Models\Country;
use App\Models\Vat;
use App\Repositories\VatRepository;
use App\Vat\EuropaClient;
use App\Vat\Support\VatHelper;
use Cache;
use Illuminate\Http\JsonResponse;
use SoapFault;

class VatController extends Controller
{
    /**
     * @var VatRepository
     */
    private $vatRepository;

    /**
     * VatController constructor.
     * @param VatRepository $vatRepository
     */
    public function __construct(VatRepository $vatRepository)
    {
        $this->vatRepository = $vatRepository;
    }

    /**
     * Check the VAT.
     *
     * @param CheckRequest $request
     * @return JsonResponse
     */
    public function check(CheckRequest $request)
    {
        try {
            $result = $this->vatRepository->findOrFetch(
                $request->get('vat_number'),
                $request->get('requester_vat_number'),
                $request->get('user_id')
            );
        } catch (SoapFault $soapFault) {
            return $this->apiError($soapFault->getMessage());
        }

        $data = $this->prepareResult($result);
        return $this->apiSuccess($data);
    }

    /**
     * Prepare the result for response.
     *
     * @param Vat|bool $response
     * @return array
     */
    private function prepareResult($response)
    {
        if ($response) {
            $data = $response->toArray();
        } else {
            $data = [];
        }

        $data['valid'] = !!$response;
        return $data;
    }
}
