<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Vat
 * @package App\Models
 *
 * @property int id
 * @property string vat_number
 * @property string trader_name
 * @property string trader_address
 * @property string trader_company_type
 * @property Carbon last_request_at
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @method static self where($column, $operator = null, $value = null, $boolean = 'and')
 * @method static self first($columns = array())
 * @method static self create($attributes = array())
 */
class Vat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vat_number', 'trader_name', 'trader_address', 'trader_company_type', 'trader_company_type', 'last_request_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'last_request_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'last_request_at', 'created_at', 'updated_at'
    ];
}
