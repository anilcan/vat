<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class Request
 * @package App\Models
 *
 * @property int id
 * @property string vat_number
 * @property int vat_id
 * @property string requester_vat_number
 * @property int requester_vat_id
 * @property string user_id
 * @property string request_file
 * @property string response_file
 * @property string identifier
 * @property Carbon created_at
 * @property Carbon updated_at
 *
 * @property-read Vat vat
 * @property-read Vat requesterVat
 *
 * @method static self create($attributes = array())
 */
class Request extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vat_country_id', 'vat_number', 'vat_id', 'requester_vat_country_id', 'requester_vat_number',
        'requester_vat_id', 'user_id', 'request_file', 'response_file', 'identifier'
    ];

    /**
     * @return BelongsTo
     */
    public function vat()
    {
        return $this->belongsTo(Vat::class, 'vat_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function requesterVat()
    {
        return $this->belongsTo(Vat::class, 'requester_vat_id', 'id');
    }
}
