<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('vat_number', 15)->index();
            $table->unsignedInteger('vat_id')->nullable()->index();
            $table->string('requester_vat_number', 15)->nullable()->index();
            $table->unsignedInteger('requester_vat_id')->nullable()->index();
            $table->string('user_id')->nullable()->index();
            $table->string('request_file', 127);
            $table->string('response_file', 127)->nullable();
            $table->string('identifier', 31)->nullable();
            $table->timestamps();

            $table->foreign('vat_id')
                ->references('id')->on('vats')
                ->onDelete('cascade')->onUpdate('cascade');

            $table->foreign('requester_vat_id')
                ->references('id')->on('vats')
                ->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
