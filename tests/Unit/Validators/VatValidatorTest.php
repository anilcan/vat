<?php

namespace Tests\Unit\Validators;

use Tests\TestCase;
use Validator;

class VatValidatorTest extends TestCase
{
    /** @test */
    public function canPassValidFormat()
    {
        $this->assertTrue(
            $this->validate('GB979053087')
        );
    }

    /** @test */
    public function canPassValidFormatWithSpaces()
    {
        $this->assertTrue(
            $this->validate('FR 82542065479')
        );
    }

    /** @test */
    public function canPassValidFormatWithSlashes()
    {
        $this->assertTrue(
            $this->validate('IE/9/E/61585W')
        );
    }

    /** @test */
    public function canPassValidFormatWithWords()
    {
        $this->assertTrue(
            $this->validate('IE47909012R')
        );
    }

    /**
     * Validate
     *
     * @param $data
     * @return bool
     */
    protected function validate($data)
    {
        return Validator::make([
            'data' => $data
        ], [
            'data' => 'vat'
        ])->passes();
    }
}
