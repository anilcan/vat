<?php

namespace Tests\Unit\Vat\Support;

use App\Vat\Support\VatHelper;
use Tests\TestCase;

class VatHelperTest extends TestCase
{
    /** @test */
    public function formatCanRemoveSpaces()
    {
        $vat = 'F R 8254 206 5479 ';
        $this->assertSame(VatHelper::formatVatNumber($vat), 'FR82542065479');
    }

    /** @test */
    public function formatCanRemoveNonWordChars()
    {
        $vat = 'F/ R 8/ 254 2/06 547|9 ';
        $this->assertSame(VatHelper::formatVatNumber($vat), 'FR82542065479');
    }

    /** @test */
    public function canParseFormattedVat()
    {
        $vat = 'GB214577410';
        $parse = VatHelper::parse($vat);
        $this->assertNotNull($parse);
        $this->assertSame($parse['countryCode'], 'GB');
        $this->assertSame($parse['vatNumber'], '214577410');
    }

    /** @test */
    public function canParseFormattedVatWithLetters()
    {
        $vat = 'IE47909012R';
        $parse = VatHelper::parse($vat);
        $this->assertNotNull($parse);
        $this->assertSame($parse['countryCode'], 'IE');
        $this->assertSame($parse['vatNumber'], '47909012R');
    }
}
