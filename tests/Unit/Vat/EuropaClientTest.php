<?php

namespace Tests\Unit\Vat;

use App\Vat\EuropaClient;
use Carbon\Carbon;
use SoapClient;
use Tests\TestCase;

class EuropaClientTest extends TestCase
{
    /** @test */
    public function clientIsBinding()
    {
        $client = app(EuropaClient::class);
        $this->assertInstanceOf(EuropaClient::class, $client);
    }

    /** @test */
    public function clientIsSettingsWsdl()
    {
        $client = app(EuropaClient::class);
        $wsdl = config('vat.wsdl');
        $this->assertSame($client->getWsdl(), $wsdl);
    }

    /** @test */
    public function clientIsCreatingSoap()
    {
        $client = app(EuropaClient::class);
        $this->assertInstanceOf(SoapClient::class, $client->getClient());
    }

    /** @test */
    public function clientIsConvertingInvalidModel()
    {
        $response = [
            'countryCode' => 'FR',
            'vatNumber' => '86443790481',
            'requestDate' => '2019-07-08+02:00',
            'valid' => false,
            'traderName' => '---',
            'traderCompanyType' => '---',
            'traderAddress' => '---',
            'requestIdentifier' => '',
        ];

        $client = app(EuropaClient::class);
        $model = $client->toCheckVatApproxModel((object)$response);

        $this->assertSame($model->countryCode, $response['countryCode']);
        $this->assertSame($model->vatNumber, $response['vatNumber']);
        $this->assertInstanceOf(Carbon::class, $model->requestDate);
        $this->assertFalse($model->valid);
        $this->assertNull($model->traderName);
        $this->assertNull($model->traderCompanyType);
        $this->assertNull($model->traderAddress);
        $this->assertNull($model->requestIdentifier);
    }

    /** @test */
    public function clientIsConvertingValidModel()
    {
        $response = [
            'countryCode' => 'FR',
            'vatNumber' => '86443790480',
            'requestDate' => '2019-07-08+02:00',
            'valid' => true,
            'traderName' => ' SARL MEDIATOURS',
            'traderCompanyType' => '---',
            'traderAddress' => '
32 RUE GUTENBERG
37300 JOUE LES TOURS ',
            'requestIdentifier' => 'WAPIAAAAWvTLelor',
        ];

        $client = app(EuropaClient::class);
        $model = $client->toCheckVatApproxModel((object)$response);

        $this->assertSame($model->countryCode, $response['countryCode']);
        $this->assertSame($model->vatNumber, $response['vatNumber']);
        $this->assertInstanceOf(Carbon::class, $model->requestDate);
        $this->assertTrue($model->valid);
        $this->assertSame($model->traderName, 'SARL MEDIATOURS');
        $this->assertNull($model->traderCompanyType);
        $this->assertSame($model->traderAddress, '32 RUE GUTENBERG
37300 JOUE LES TOURS');
        $this->assertSame($model->requestIdentifier, 'WAPIAAAAWvTLelor');
    }

    /** @test */
    public function clientIsConvertingCarbon()
    {
        $date = '2019-07-08+02:00';

        /** @var Carbon $carbon */
        $carbon = Carbon::createFromFormat(EuropaClient::DATE_FORMAT, $date);
        $this->assertSame($carbon->year, 2019);
        $this->assertSame($carbon->month, 7);
        $this->assertSame($carbon->day, 8);
        $this->assertSame($carbon->timezone->getOffset($carbon), 7200);
    }
}
