<?php

namespace Tests\Feature\Vat;

use App\Vat\EuropaClient;
use SoapFault;
use Tests\TestCase;

class EuropaClientTest extends TestCase
{
    /** @test */
    public function clientCanCheckValidVat()
    {
        $client = app(EuropaClient::class);
        $model = $client->checkVatApprox('GB', '979053087');
        $this->assertTrue($model->valid);
    }

    /** @test */
    public function clientCanCheckInvalidVat()
    {
        $client = app(EuropaClient::class);
        $model = $client->checkVatApprox('GB', '123456789');
        $this->assertFalse($model->valid);
    }

    /** @test */
    public function clientCanCheckValidVatWithValidVat()
    {
        $client = app(EuropaClient::class);
        $model = $client->checkVatApprox('GB', '979053087', 'GB', '979053087');
        $this->assertTrue($model->valid);
    }

    /** @test */
    public function clientCanCheckInvalidVatWithValidVat()
    {
        $client = app(EuropaClient::class);
        $model = $client->checkVatApprox('GB', '123456789', 'GB', '979053087');
        $this->assertFalse($model->valid);
    }

    /** @test */
    public function clientCannotCheckValidVatWithInvalidVat()
    {
        $this->expectException(SoapFault::class);
        $client = app(EuropaClient::class);
        $client->checkVatApprox('GB', '979053087', 'GB', '123456789');
    }

    /** @test */
    public function clientCanCheckInvalidVatWithInvalidVat()
    {
        $client = app(EuropaClient::class);
        $model = $client->checkVatApprox('FR', '979053087', 'GB', '123456789');
        $this->assertFalse($model->valid);
    }
}
