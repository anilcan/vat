<?php

return [
    'wsdl' => env('VAT_WSDL', 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl'),
    'default_country_code' => env('VAT_DEFAULT_COUNTRY_CODE', 'FR'),
    'default_vat_number' => env('VAT_DEFAULT_NUMBER', '95481749687'),
    'cache_ttl' => 60 * 60 * 6
];
